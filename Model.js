module.exports = Model;


function Model (dimacs) {
    var lines = dimacs.split("\n");
    var i;
    var clauses = [];
    var vars, expectedclauses;
    var literals = [];
    for (i = 0; i < lines.length; i++) {
        if (lines[i].endsWith("\n")) {
            lines[i] = lines[i].slice(0,lines[i].length-1);
        }
        if (lines[i] == "") continue;
        var clause = lines[i].trim().split(" ");
        console.log("parsing clause '" + clause + "'" );
        switch (clause[0]) {
        case 'c':
            break;
        case 'p':
            if (clause[1] != 'cnf') {
                console.log('Parse failure: non-CNF format specified');
            }
            vars = Number(clause[2]);
            expectedclauses = Number(clause[3]);
            break;
        default:
            var lits = clause.map(Number);
            if (lits[lits.length - 1] != 0) {
                literals = literals.concat(lits);
            } else {
                clauses.push(literals.concat(lits.slice(0,lits.length-1)));
                literals = [];
            }
            break;
        }
    }
    this.status = 'UNKNOWN';
    this.vars = vars;
    this.formula = clauses;
    this.assignment = new Array(vars+1).fill(0);
}



Model.prototype.SAT = function(){
    return this.status == 'SAT'
};
Model.prototype.UNSAT = function(){
    return this.status == 'UNSAT'
};
Model.prototype.values = function(){
    var assigned = new Array(this.vars); 
    var i;
    for(i=1;i<this.assignment.length;i++){
        switch(this.assignment[i]) {
        case -1: 
            assigned[i-1] = "-" + i; break;
        case  0: 
            assigned[i-1] = "?" + i; break;
        case  1: 
            assigned[i-1] = "+" + i; break;
        }
    }
    return assigned.join(' ');
};
Model.prototype.numClauses = function() {return this.formula.length;};
Model.prototype.numVars = function() {return this.vars;};
Model.prototype.assign = function(index,value) {
    if (value) {
        this.assignment[index] = 1;
    } else {
        this.assignment[index] = -1;
    }
}
Model.prototype.isAssigned = function (index) {
    return this.assignment[index] != 0;
}
Model.prototype.clause = function(index) {
    return this.formula[index];
}
Model.prototype.literal = function(cl,ind) {
    return this.formula[cl][ind];
}