//var Model = require("./Model");

function copyModel(obj) {
  this.status = obj.status;
  this.vars = obj.vars;
  this.formula = new Array(obj.numClauses());
	var i; for (i = 0; i < obj.numClauses(); i++) {
		this.formula[i] = obj.formula[i].slice();
	}
  this.assignment = obj.assignment.slice();
	this.SAT = obj.SAT;
	this.UNSAT = obj.UNSAT;
	this.values = obj.values;
	this.numClauses = obj.numClauses;
	this.numVars = obj.numVars;
	this.assign = obj.assign;
	this.isAssigned = obj.isAssigned;
	this.clause = obj.clause;
	this.literal = obj.literal;
}

function solve(model) {
	return dpll(model);
}

function dpll(model) {
	//console.log("***  begin DPLL  ***")
	//console.log("clauses: " + model.numClauses() + ", variables: " 
	//						+ model.numVars())
	//console.log("initial assignment: " + model.assignment);
	
	var count = 0;
	do {
		var pos = new Array(model.vars).fill(false);
		var neg = new Array(model.vars).fill(false);
		count = 0;
		var i;
		var clauses = [];
		var choice;
		//console.log("** unit propagation **");
		
		loopclauses:
		for (i=0;i<model.numClauses();i++) {
			var clause = [];
			var j;
			
			loopliterals:
			for (j=0;j<model.clause(i).length;j++) {
				var literal = model.literal(i,j);
				var val = literal * model.assignment[Math.abs(literal)];
				if (val > 0) {
					// literal evaluates to TRUE
					//console.log(literal + "==TRUE: dropping clause " 
					//						+ model.clause(i));
					continue loopclauses; // this clause is always satisfied
				} else if (val == 0) {
					// value of literal unknown
					clause.push(literal);
					if ( literal > 0 ) {
						pos[literal] = true;
					} else {
						neg[-1*literal] = true;
					}
				}
			}
			if (clause.length == 0) {
				//console.log("UNSAT: conflict clause " + model.clause(i));
				model.status = 'UNSAT';
				return model;
			}
			if (clause.length == 1) {
				//console.log(clause[0] + "<- TRUE: unit clause" + model.clause(i));
				model.assignment[Math.abs(clause[0])] = clause[0] > 0 ? 1 : -1;
				count++;
			} else {
				//console.log("clause: " + clause);
				clauses.push(clause);
			}
		}
		// end unit prop
		//console.log("variables assigned: " + count);
		//console.log("clauses eliminated: " 
		//						+ (model.numClauses() - clauses.length));
		model.formula = clauses;
		if (clauses.length == 0) {
			//console.log("SAT: all clause satisfied")
			model.status = 'SAT';
			return model;
		}
		
		//console.log("** eliminating pure literals **");
		var count2 = 0;
		choice = 0;
		for (i = 1; i <= model.numVars(); i++) {
			if (!model.isAssigned(i)) {
				if (pos[i] && !neg[i]) {
					//console.log(i + " <- TRUE: pure literal (positive)")
					model.assign(i,true);
					count2++;
				} else if (!pos[i] && neg[i]) {
					//console.log(i + " <- FALSE: pure literal (negative)")
					model.assign(i,false);
					count2++;
				} else {
					choice = i;
				}
			}
		}
		//console.log("variables assigned: " + count2);
	} while (count + count2 > 0);
	if (choice) {
		//console.log("*** Branching: %d == TRUE", choice)
		var modelT = new copyModel(model);
		modelT.assign(choice,true);
		var modelT = dpll(modelT);
		if (modelT.SAT()) {
			return modelT;
		}
		//console.log("*** Branching: %d == FALSE", choice)
		var modelF = new copyModel(model);
		modelF.assign(choice,false);
		return dpll(modelF);
	}
	console.log("Umm, you shouldn't be here.")
	return model;
}
exports.solve = solve;