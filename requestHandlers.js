var querystring = require("querystring");
var solver = require("./solver");
var Model = require("./Model");

function start(response, postData) {
    console.log("Request handler 'start' was called.");
    
    var body = '<html>'+ 
        '<head>'+
        '<meta http-equiv="Content-Type" content="text/html; '+
        'charset=UTF-8" />'+
        '</head>'+
        '<body>'+
        '<form action="/solve" method="post">'+
        '<textarea name="text" rows="20" cols="60"></textarea>'+
        '<input type="submit" value="Submit text" />'+
        '</form>'+
        '</body>'+
        '</html>';
        
    response.writeHead(200, {"Content-Type" : "text/html"});
    response.write(body);
    response.end();
}

function solve(response, postData) {
    console.log("Request handler 'solve' was called.");
    response.writeHead(200, {"Content-Type" : "text/plain"});
    var model = new Model(querystring.parse(postData).text);
    response.write("variables: " + model.numVars() + ", clauses: " + model.numClauses() + "\n");
    console.time('solution');
    var result = solver.solve(model);
    console.timeEnd('solution');
    response.write(result.status + "\n");
    response.write(result.values());
    response.end();
}

exports.start = start;
exports.solve = solve;